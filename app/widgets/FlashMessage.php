<?php

namespace App\widgets;

use App;

/**
 * Flash message widget using Bootstrap Alert Component
 */
class FlashMessage
{
    /**
     * Available types of messages
     * @var const TYPES
     */
    protected const TYPES = ['error','success','info','warning'];

    /**
     * Constructor
     * Foreach flash session and render if message exist
     */
    public function __construct()
    {
        foreach (self::TYPES as $type) {
            $message = App::$session->getFlash($type);

            if ($message) {
                $this->render($type, $message['message']);
            }
        }        
    }

    /**
     * Render message alert
     * @param $type Message type
     * @param string $message Message
     * @return void
     */
    protected function render(string $type, string $message): void
    {
        switch ($type) {
            case 'error':
                $class = 'danger';
                break;
            case 'success':
                $class = 'success';
                break;
            case 'info': 
                $class = 'info';
                break;
            case 'warning':
                $class = 'warning';
                break;
            default:
                $class = 'info';
        }

        ob_start();
        ?>
        <div class="alert alert-<?php echo $class?>" role="alert">
            <?php echo $message?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
        $output = ob_get_contents();
        ob_get_clean();

        echo $output;
    }
}