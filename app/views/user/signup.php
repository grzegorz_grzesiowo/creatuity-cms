<h3>User Signup</h3>
<form id="user-form" method="post" action="/signup">
    <div class="form-group">
        <label for="username">Username *</label>
        <input type="text" name="username" class="form-control" id="username" placeholder="Enter username" required minLength="8">
    </div>
    <div class="form-group">
        <label for="password">Password *</label>
        <input type="password" name="password" class="form-control" id="email" placeholder="Enter password" required minLenght="8">
    </div>
    <div class="row">
        <div class="col text-right">
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
    </div>
</form>