<div class="articles-list">
    <div class="row">
        <div class="col">
            <?php foreach($articles as $article):?>
            <div class="article">
                <h5><?php echo $article['title']?></h5>
                <div class="date"><?php echo date('d.m.Y H:i', $article['created_at'])?> by <?php echo $article['username']?></div>
                <p><?php echo substr(strip_tags($article['content']), 0, 500)?>...</p>
                <div class="row">
                    <div class="col text-right">
                        <a href="/read?id=<?php echo $article['id']?>" class="btn btn-sm btn-light">Read more</a>
                    </div>
                </div>                
            </div>
            <?php endforeach ?>
            <?php if (count($articles) == 0):?>
            <h6>There's no articles</h6>
            <?php endif?>
        </div>
    </div>
</div>