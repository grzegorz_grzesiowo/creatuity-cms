<div class="single-article">
    <div class="row">
        <div class="col-md-6">
            <h2><?php echo $title?></h2>
        </div>
        <div class="col-md-6 text-right">
            <?php if ($user_id == App::$user->getId()) :?>
            <a href="/article/update?id=<?php echo $id?>" class="btn btn-sm btn-light"><i class="fas fa-edit"></i></a>
            <a onclick="confirm('Do you really want to delete this article?')" href="/article/delete?id=<?php echo $id?>" class="btn btn-sm btn-light"><i class="fas fa-trash"></i></a>
            <?php endif ?>
        </div>
    </div>
    <div class="date"><?php echo date('d.m.Y H:i', $created_at)?> by <?php echo $username?></div>
    <p><?php echo $content?></p>
</div>