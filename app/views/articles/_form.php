<form method="post" id="article-form">
    <div class="form-group">
        <label for="title">Title *</label>
        <input type="text" name="title" class="form-control" id="title" placeholder="Enter title" required minLength="8" <?php if ($mode=='update'): ?>value="<?php echo $data['title']?>"<?php endif ?> ?>
    </div>
    <div class="row">
        <div class="col">
            <textarea name="content" id="article-content-editor" rows="20" required><?php echo App::$request->post('content')?><?php if ($mode=='update'): ?><?php echo $data['content']?><?php endif ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col text-right">
            <input type="hidden" name="user_id" value="<?php echo App::$user->data('id')?>"/>
            <button type="submit" class="btn btn-lg btn-primary">SAVE</button>
        </div>
    </div>
</form>