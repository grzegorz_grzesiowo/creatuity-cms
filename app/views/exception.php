<h1>Error <?php echo $exception->getCode()?></h1>
<p><?php echo $exception->getMessage()?></p>
<?php if (DEV):?>
<p>In <?php echo $exception->getFile()?>(<?php echo $exception->getLine()?>)</p>
<h5>Stack trace</h5>
<code><?php echo $exception->getTraceAsString()?></code>
<?php endif ?>