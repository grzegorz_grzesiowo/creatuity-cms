<?php

namespace app\models;

use Gsw\DatabaseMysql;
use \PDO;
use App;

/**
 * Articles model
 * 
 * @author Grzegorz Marszał <grzegorz@grzesiowo.pl>
 */
class Article extends DatabaseMysql
{
    /**
     * Update article with specified data
     * 
     * @param int $id Article id
     * @param array $data Article data
     * @return bool
     */
    public function updateArticle(int $id, array $data): bool 
    {
        if (!isset($data['title']) || !isset($data['content'])) {
            throw new \Exception('Fields title & content are required.', 406);
        }  
        
        $stmt = $this->getConnection()->prepare('UPDATE `articles` SET 
            `title` = :title,
            `content` = :content,
            `updated_at` = :updated_at
            WHERE id = :id');

        $stmt->bindValue(':title', $data['title'], PDO::PARAM_STR);
        $stmt->bindValue(':content', $data['content'], PDO::PARAM_STR);
        $stmt->bindValue(':updated_at', time(), PDO::PARAM_INT);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        return $stmt->execute();        
    }

    /**
     * Create new article
     * 
     * @param array $data New article data
     * @param int $user_id Current user ID
     * @return bool
     */
    public function createArticle(array $data): bool
    {
        if (!isset($data['title']) || !isset($data['content'])) {
            throw new \Exception('Fields title & content are required.', 406);
        }       
     
        $stmt = $this->getConnection()->prepare('INSERT INTO `articles` 
            (`title`, `user_id` , `content`, `created_at`, `updated_at`) VALUES (:title, :user_id, :content, :created_at, :updated_at)');
        $stmt->bindValue(':title', $data['title'], PDO::PARAM_STR);
        $stmt->bindValue(':user_id', App::$user->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':content', $data['content'], PDO::PARAM_STR);
        $stmt->bindValue(':created_at', time(), PDO::PARAM_INT);
        $stmt->bindValue(':updated_at', time(), PDO::PARAM_INT);

        return $stmt->execute();
    }

    /**
     * Delete article
     * 
     * @param int $id Article id
     * @return bool
     */
    public function deleteArticle(int $id): bool
    {
        $stmt = $this->getConnection()->prepare('DELETE FROM `articles` WHERE `id` = :id');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        return $stmt->execute();
    }

    /**
     * Get all articles
     */
    public function getAll(): array
    {
        $stmt = $this->getConnection()->prepare('SELECT articles.id, articles.title, articles.content, articles.created_at, users.username
            FROM `articles` 
            LEFT JOIN `users` 
            ON articles.user_id = users.id
            ORDER BY `created_at` DESC');

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);        
    }

    /**
     * Get one article
     * 
     * @param int $id Article id
     * @return array Array of article fields
     */
    public function getOne(int $id): array
    {       
        $stmt = $this->getConnection()->prepare('SELECT articles.id, articles.user_id, articles.title, articles.content, articles.created_at, users.username
            FROM `articles` 
            LEFT JOIN `users` 
            ON articles.user_id = users.id
            WHERE articles.id = :id');

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

}