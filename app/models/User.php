<?php

namespace app\models;

use Gsw\DatabaseMysql;
use \PDO;

/**
 * Users model with some helpful metods to manipulate list: addNew, getOne, delete etc.
 * 
 * @author Grzegorz Marszał <grzegorz@grzesiowo.pl>
 */
class User extends DatabaseMysql
{
    /**
     * Create user with specified data
     * 
     * @param array $data New user data
     * @return bool
     */
    public function createUser(array $data): bool
    {        
        $stmt = $this->getConnection()->prepare('INSERT INTO `users` (`username`, `password`) VALUES(:username, :password)');
        $stmt->bindValue(':username', $data['username'], PDO::PARAM_STR);
        $stmt->bindValue(':password', sha1($data['password']), PDO::PARAM_STR);

        return $stmt->execute();        
    }

    /**
     * Check user identity by username & password
     * 
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function checkIdentity(string $username, string $password): bool
    {
        $stmt = $this->getConnection()->prepare('SELECT * FROM `users` WHERE `username` = :username AND `password` = :password');
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
        $stmt->bindValue(':password', sha1($password), PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchColumn() !== false ? true : false;
    }

    /**
     * Find identity by given username
     * 
     * @param string $username
     * @return mixed
     */
    public function findIdentityByUsername(string $username)
    {
        $stmt = $this->getConnection()->prepare('SELECT `id`, `username` FROM `users` WHERE `username` = :username');
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Check if user exist (by username)
     * 
     * @param string $username
     * @return bool
     */
    private function checkIfUserExists($username): bool
    {     
        $stmt = $this->getConnection()->prepare('SELECT * FROM `users` WHERE `username` = :username');
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchColumn() !== false ? true : false;
    }
}