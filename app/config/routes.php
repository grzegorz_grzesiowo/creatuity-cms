<?php
/**
 * Routes 
 * 
 * @example 'update' => 'users/update' => http://youdomain.com/update will call UsersControler and actionUpdate
 */
return [
    ''              => 'articles/index',
    'read'          => 'articles/read',
    'article/create'=> 'articles/create',
    'article/update'=> 'articles/update',
    'article/delete'=> 'articles/delete',
    'signin'        => 'user/signin',
    'signup'        => 'user/signup',
    'logout'        => 'user/logout'
];