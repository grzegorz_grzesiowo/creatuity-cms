<?php
use function DI\create;
use DI\Container;

return [
    'routes' => require 'routes.php',    
    'router' => function (Container $c) {
        return new \Gsw\Router($c->get('routes'));
    },
    'view' => create(\Gsw\View::class),
    'user' => create(\Gsw\User::class),
    'request' => create(\Gsw\Request::class),
    'session' => create(\Gsw\Session::class)
];