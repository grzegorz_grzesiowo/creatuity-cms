<?php

namespace app\controllers;

use App;
use Gsw\Controller;
use app\models\User;

class UserController extends Controller
{
    /**
     * User sign up action
     */
    public function actionSignup()
    {
        if (App::$request->isPost()) {
            $user = new User();

            if ($user->findIdentityByUsername(App::$request->post('username'))) {
                App::$session->setFlash('error', "User " . App::$request->post('username') . ' exists');
                $this->redirect('/signup');
            }
            
            $create = $user->createUser(App::$request->post());            
            
            if ($create) {
                App::$session->setFlash('success', 'User account created! Now you can <a href="/signin" class="btn btn-sm btn-primary">Sign In</a>');
            } else {
                App::$session->setFlash('error', "Can't create user, please try again.");
            }

            return $this->redirect('/signup');
        }

        $this->view->render('user/signup', []);
    }

    /**
     * User sign in action
     */
    public function actionSignin()
    {
        if (App::$request->isPost()) {

            if (!App::$request->post('username') || !App::$request->post('password')) {
                App::$session->setFlash('error', "Username & password are required");
                return $this->redirect('/');
            }
            
            $user = new User();

            if ($user->checkIdentity(App::$request->post('username'), App::$request->post('password'))) {
                $identity = $user->findIdentityByUsername(App::$request->post('username'));

                App::$session->set('user', $identity);
                App::$session->setFlash('success', "Nice to see you again " . $_POST['username'] . '!');

                return $this->redirect('/');
            }
            
            App::$session->setFlash('error', "Invalid login or password");
            return $this->redirect('/signin');
        }

        $this->view->render('user/signin', []);
    }

    /**
     * Logout user action
     */
    public function actionLogout()
    {
        if (App::$user->isLoggedIn()) {
            App::$session->delete('user');
            App::$session->setFlash('success', "Now you're logged out!");
            return $this->redirect('/');
        }

        App::$session->setFlash('error', "You're not logged in");
        return $this->redirect('/');
    }

}