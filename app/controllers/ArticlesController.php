<?php

namespace App\controllers;

use App;
use Gsw\Controller;
use Gsw\web\FlashMessage;
use app\models\Article;
use app\models\User;

/**
 * Articles controler create, update, display list, delete
 * 
 * @author Grzegorz Marszal <grzegorz@grzesiowo.pl>
 */
class ArticlesController extends Controller
{
    /**
     * Render article list
     */
    public function actionIndex()
    {
        $articles = new Article();

        return $this->view->render('articles/index', [
            'articles' => $articles->getAll()
        ]);
    }

    /**
     * Create new article
     */
    public function actionCreate()
    {
        if (!App::$user->isLoggedIn()) {
            throw new \Exception("You don't have access to create article", 401);
        }

        if (App::$request->isPost()) {
            $model = new Article();            
            $save = $model->createArticle($_POST);

            if ($save) {
                App::$session->setFlash('success', "Article created!");
                return $this->redirect('/');
            }

            App::$session->setFlash('error', "Can't create article, please try again.");
        }

        return $this->view->render('articles/create', [
            'mode' => 'create'
        ]);
    }

    /**
     * Update article
     */
    public function actionUpdate()
    {
        if (!App::$request->get('id')) {
            throw new \Exception("Missing param ID", 406);
        }

        $model = new Article();
        $article = $model->getOne(App::$request->get('id'));

        if (!$article) {
            throw new \Exception("Can't find article", 404);
        }

        if ($article['user_id'] != App::$user->data('id')) {
            throw new \Exception("You don't have access to update this article", 401);
        }

        if (App::$request->isPost()) {           
            $save = $model->updateArticle($article['id'], $_POST);

            if ($save) {
                App::$session->setFlash('success', "Article updated!");
                $this->redirect('/article/update?id=' . App::$request->get('id'));
            } 

            App::$session->setFlash('error', "Can't update article.");            
        }

        return $this->view->render('articles/update', [
            'mode' => 'update',
            'data' => $article
        ]);      
        
    }

    /**
     * Read article
     */
    public function actionRead()
    {
        if (!App::$request->get('id')) {
            throw new \Exception("Missing param ID", 406);
        }        

        $model = new Article();
        $article = $model->getOne(App::$request->get('id'));

        if (!$article) {
            throw new \Exception("Can't find article", 404);
        }

        return $this->view->render('articles/article', $article);                    
    }

    /**
     * Delete article
     */
    public function actionDelete()
    {
        if (!App::$request->get('id')) {
            throw new \Exception("Missing param ID", 406);
        }   

        $model = new Article();
        $find = $model->getOne(App::$request->get('id'));

        if (!$find) {
            throw new \Exception("Can't find article", 404);
        }

        if ($find['user_id'] !== App::$user->getId()) {
            throw new \Exception("You don't have access to delete this article", 401);
        }

        $delete = $model->deleteArticle(App::$request->get('id'));

        if ($delete) {
            App::$session->setFlash('success', "Article " . $find['title'] . " deleted!");
            $this->redirect('/');  
            
        } else {
            throw new \Exception("Can't delete article " .  $find['title'], 500);
        } 
    }
}