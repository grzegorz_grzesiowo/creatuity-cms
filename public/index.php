<?php
session_start();

//Application root directory
define('APP_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/' );

//Views path
define('VIEWS_PATH', APP_ROOT . 'app/views/');

//Default layout file
define('DEFAULT_LAYOUT', 'main');

//Do some more development things like extended Error exception etc.
define('DEV', true);

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);
set_error_handler('Gsw\Error::errorHandler');
set_exception_handler('Gsw\Error::exceptionHandler');


(new App(require APP_ROOT . 'app/config/app.php'))->run();