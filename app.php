<?php

class App
{
    public static $session;
    public static $user;
    public static $request;
    public static $container;

    public function __construct($config)
    {                
        $container = $this->initDiContainer();
        
        self::$container = $container;
        self::$session = $container->get('session');
        self::$user = $container->get('user');
        self::$request = $container->get('request');
    }

    private function initDiContainer()
    {
        $containerBuilder = new \DI\ContainerBuilder();
        $containerBuilder->addDefinitions(APP_ROOT . 'app/config/app.php');
        return $containerBuilder->build();
    }

    public function run()
    {
        self::$container->get('router')->run();
    }
}