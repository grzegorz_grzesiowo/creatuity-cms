<?php

namespace Gsw;

use Gsw\helpers\UrlHelper;
use DI;

/**
 * Simple router 
 * 
 * @example http://domain.com/category/index?p=1 will trigger CategoryController and actionIndex and pass parameter "p" to controller
 */
class Router
{
    /**
     * Some parameters from url like controller, action etc.
     * @var array of parameters
     */
    protected $urlParams;

    /**
     * Array with routes used in project
     * @var array Routes array
     */
    protected $routes;

    /**
     * Single route
     */
    protected $route;

    /**
     * Constructor 
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * Run router
     */
    public function run(): void
    {
        $this->parseUrl();

        if (!$this->routeExists($this->route)) {
            throw new \Exception("ROUTE DOESN'T EXISTS", 404);
        }

        $controller_action = $this->extractFromRoute($this->routes[$this->route]);
        $controller = 'App\controllers\\' . ucfirst($this->convertToCamelCase($controller_action['controller'])) . 'Controller';

        if (class_exists($controller)) {
            $action_name = ucfirst($this->convertToCamelCase($controller_action['action']));

            $controller_object = new  $controller(array_merge(
                $this->urlParams, [
                    'controller' => $controller_action['controller'],
                    'action' => $controller_action['action']   
                ]
            ), \App::$container->get('view'));

            $controller_object->$action_name();
        } else {
            throw new \Exception("CONTROLLER $controller DOESN'T EXISTS", 404);
        }

    }

    /**
     * Convert string to camelCase
     * 
     * @param string    $string string to convert to camelCase
     * @example         "example-controller" will be converted to "ExampleController"
     */
    protected function convertToCamelCase(string $string): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $string))));
    }

    /**
     * Extract route & url params from URL
     */
    protected function parseUrl(): void
    {
        $cur_url = UrlHelper::currentUrl();
        $domain = UrlHelper::extractDomain($cur_url);
        
        $explode = explode('?', str_replace($domain . '/', '', $cur_url));
        $this->route = $explode[0];

        $query_string = parse_url($cur_url, PHP_URL_QUERY);
        parse_str($query_string, $query_params);

        $this->urlParams = $query_params;
    }

    /**
     * Check if route exists in routes array
     * 
     * @param string $name Route name
     * @return bool
     */
    protected function routeExists(string $name): bool
    {
        return array_key_exists($name, $this->routes) ? true : false;
    }

    /**
     * Extract all you need from router
     * @param string $route Route
     * @example users/list will return ['controller'=>'users', 'action'=>'list]
     */
    protected function extractFromRoute(string $route): array
    {
        $explode = explode('/', $route);

        if (count($explode) != 2) {
            throw new \Exception("Invalid route $route", 500);
        }

        return [
            'controller' => $explode[0],
            'action' => $explode[1]    
        ];
    }
}