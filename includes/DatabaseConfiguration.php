<?php

namespace Gsw;

use Gsw\DatabaseConfigurationInterface;

/**
 * Database configuration class
 */
class DatabaseConfiguration implements DatabaseConfigurationInterface
{
    /**
     * @var string $host
     */
    private $host;

    /**
     * @var string $username
     */
    private $username;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var string $database Database name
     */
    private $database;

    /**
     * Constructor
     * 
     * @param array $config Configuration data: host, database, username, password
     */
    public function __construct(array $config)
    {
        $this->host = $config['host'];
        $this->database = $config['database'];
        $this->username = $config['username'];
        $this->password = $config['password'];
    }

    /**
     * Get host
     * 
     * @return string Host
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Get username
     * 
     * @return string Username
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Get password
     * 
     * @return string Password
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Get Database name
     * 
     * @return string Database name
     */
    public function getDatabase(): string
    {
        return $this->database;
    }
}