<?php

namespace Gsw\helpers;

/**
 * Some useful functions for URLs
 */
class UrlHelper
{
    /**
     * Extract domain from url string
     * @param string    $url   
     * @example         http://example-domain.com/page/example?p=1 will be converted to http://example-domain.com
     */
    public static function extractDomain($url): string
    {
        $_url = parse_url($url);       
        $output =  $_url['scheme'] . '://' . $_url['host'];
        if (isset($_url['port']) && $_url['port'] != 80) {
            $output .= ':' . $_url['port'];
        }

        return $output;
    }

    /**
     * Return current page URL
     */
    public static function currentUrl(): string
    {
        $output = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        return $output;
    }
}