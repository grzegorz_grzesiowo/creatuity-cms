<?php

namespace Gsw;

/**
 * Request class with some helpful stuff
 */
class Request
{
    /**
     * Get $_GET value
     * 
     * @param string $key Key for $_GET
     * @return mixed
     */
    public function get(string $key = null)
    {
        if ($key == null) {
            return $_GET;
        }

        return $_GET[$key] ?? null;
    }

    /**
     * Get $_POST value
     * 
     * @param string $key Key for $_POST
     * @return mixed
     */
    public function post(string $key = null)
    {
        if ($key == null) {
            return $_POST;
        }

        return $_POST[$key] ?? null;
    }

    /**
     * Check if request method is POST
     * @return bool
     */
    public function isPost(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST' ? true : false;
    }

    /**
     * Check if request method is GET
     * @return bool
     */
    public function isGet(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET' ? true : false;
    }

    /**
     * Check if we got ajax request
     * @return bool
     */
    public function isAjax(): bool
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ? true : false;
    }
}