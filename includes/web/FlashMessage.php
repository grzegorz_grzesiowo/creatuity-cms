<?php

namespace Base\web;

class FlashMessage
{
    public function __construct(string $message, string $type)
    {
        $this->removeFlashMessage();

        $this->addFlashMessage($message, $type);
    }

     /**
     * Add flash message stored in SESSION
     * 
     * @param string $message Message content
     * @param string $type Message type (error, success, info etc.)
     */
    public function addFlashMessage(string $message, string $type = 'info')
    {
        list(
            $_SESSION['flash'],
            $_SESSION['flash']['message'], 
            $_SESSION['flash']['type'], 
            $_SESSION['flash']['seen']
        ) = [[], $message, $type, false];
    }

    /**
     * Remove flash message from session
     */
    protected function removeFlashMessage()
    {
        if ($this->messageExists()) {
            if ($this->messageSeen()) {
                unset($_SESSION['flash']);
            } else {
                $_SESSION['flash']['seen'] = true;
            }
        }
    }

    /**
     * Check if message exists
     * @return bool
     */
    protected function messageExists(): bool
    {
        return isset($_SESSION['flash']) ? true : false;
    }


    protected function messageSeen(): bool
    {
        return isset($_SESSION['flash']) && $_SESSION['flash']['seen'] ? true : false;
    }

    public function __destruct()
    {
        $this->removeFlashMessage();
    }
}