<?php

namespace Gsw;

/**
 * View Interface
 */
interface ViewInterface
{
    /**
     * Render method
     * 
     * @param string $view View file ex. articles/index
     * @return void
     */
    public function render(string $view, array $params): void;

    /**
     * Set layout
     * 
     * @param string $name Layout file name ex. main
     * @return void
     */
    public function setLayout(string $name): void;
}