<?php

namespace Gsw;

use App;

class User
{
    /**
     * Check if user is logged in
     * @return bool 
     */
    public function isLoggedIn(): bool
    {
        return isset($_SESSION['user']) ? true : false;
    }

    /**
     * Get userdata from session
     * 
     * @param string $key
     * @return mixed
     */
    public function data(string $key)
    {
        $get = App::$session->get('user');
        return $get[$key] ?? null;
    }

    /**
     * Get current User ID
     * 
     * @return int User ID
     */
    public function getId(): ?int
    {
        $id = App::$session->get('user')['id'];
        return $id ? $id : null;
    }
}