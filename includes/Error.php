<?php
namespace Gsw;

use App;

/**
 * Custom error exception
 */
class Error
{
    /**
     * Error handler
     *
     * @param int $level  Error level
     * @param string $message  Message
     * @param string $file  Filename
     * @param int $line  Line number
     *
     * @return void
     */
    public static function errorHandler($level, $message, $file, $line)
    {
        if (error_reporting() !== 0) { 
            throw new \ErrorException($message, 0, $level, $file, $line);
        }
    }
    
    /**
     * Exception handler
     *      
     * @param Exception $exception Exception
     */
    public static function exceptionHandler($exception)
    {
        $code = $exception->getCode();

        if (is_numeric($code)) {
            http_response_code($code);
        } else {
            http_response_code(500);
        }
        
        return App::$container->get('view')->render('exception', [
            'exception' => $exception
        ]);
    }
}