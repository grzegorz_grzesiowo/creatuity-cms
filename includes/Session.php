<?php 

namespace Gsw;

class Session
{
    /**
     * @var $session with $_SESSION 
     */
    protected $session;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->session = $_SESSION;
    }

     /**
     * Add flash message stored in SESSION
     * 
     * @param string $message Message content
     * @param string $type Message type (error, success, info etc.)
     * @return void
     */
    public function setFlash(string $type, string $message): void
    {
        list(
            $_SESSION['flash'],
            $_SESSION['flash'][$type],
            $_SESSION['flash'][$type]['message']
        ) = [[], [], $message, false];
    }

    /**
     * Get flash message by type
     * 
     * @param string $type Type of flash message ex. error, success etc.
     * @return array|null 
     */
    public function getFlash(string $type): ?array
    {
        $session = $_SESSION['flash'][$type] ?? null;
        
        if ($session !== null) {
            unset($_SESSION['flash'][$type]);
        }

        return $session;
    }

    /**
     * Check if message exists
     * 
     * @return bool
     */
    protected function flashExists(): bool
    {
        return isset($_SESSION['flash']) ? true : false;
    }

    /**
     * Get session by key
     * 
     * @param string $key Session key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->session[$key] ?? null;
    }

    /**
     * Set session
     * 
     * @param string $key Session key
     * @param mixed $value Session value
     * @return void
     */
    public function set(string $key, $value): void
    {
        $_SESSION[$key] =  $value;
    }

    /**
     * Delete session by key
     * 
     * @param string $key Session key
     * @return void
     */
    public function delete(string $key): void
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

}