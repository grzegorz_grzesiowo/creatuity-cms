<?php

namespace Gsw;

/**
 * Abstract Controller Class, base for other controllers
 */
abstract class Controller
{
    /**
     * @var string Controller name
     */
    protected $controller;

    /**
     * @var string Action name
     */
    protected $action;

    /**
     * @var View $type
     */
    protected $view;

    /**
     * @param array $params Array with parameters extracted from URL
     * @param View
     */
    public function __construct($params, View $view)
    {
        $this->controller = $params['controller'];
        $this->action = $params['action'];
        $this->view = $view;
    }

    /**
     * Magic metod which is triggered when invoking inaccessible method
     */
    public function __call($action_name, $args)
    {
        $method = 'action' . $action_name;
     
        if (method_exists($this, $method)) {
            if ($this->beforeAction(lcfirst($action_name)) !== false) {
                call_user_func_array([$this, $method], $args);
                $this->afterAction(lcfirst($action_name));
            }
        } else {
            throw new \Exception("Method $method not found in controller " . $this->controller, 500);
        }
    }

    /**
     * Action called before calling target action
     * 
     * @param string $action current called action
     * @example index, getUsers etc.
     */
    public function beforeAction($action)
    {

    }

    /**
     * Action called after calling target action
     * 
     * @param string $action current called action
     * @example index, getUsers etc.
     */
    public function afterAction($action)
    {

    }

    /**
     * Redirect to URL
     * 
     * @param string $url Target URL
     */
    public function redirect(string $url)
    {
        header("Location: $url");
        die();
    }

}