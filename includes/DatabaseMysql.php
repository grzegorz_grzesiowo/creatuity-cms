<?php

namespace Gsw;

use Gsw\Database;
use \PDO;

/**
 * Mysql database class
 */
class DatabaseMysql extends Database
{
    /**
     * Static property storing PDO connection  object
     * 
     * @var PDO $connection
     */
    private static $connection = null;

    /**
     * Get existing connection or create new
     */
    protected function getConnection(): PDO
    {
        if (self::$connection === null) {
            $dsn = 'mysql:host=' . $this->config->getHost() . ';dbname=' . $this->config->getDatabase() . ';charset=utf8';

            try {
                self::$connection = new PDO($dsn, $this->config->getUsername(), $this->config->getPassword());
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            } catch (PDOException $error) {
                new \Exception($error->getMessage(), $error->getCode());
            }
        }

        return self::$connection;
    }
}