<?php

namespace Gsw;

/**
 * Abstract Database class, parent for other database classess
 */
abstract class Database
{
    /**
     * Database configuration
     * 
     * @var DatabaseConfigurationInterface
     */
    protected $config;

    public function __construct()
    {
        $this->config = new DatabaseConfiguration(require APP_ROOT . 'app/config/db.php');
    }
}