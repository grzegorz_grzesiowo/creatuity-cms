<?php

namespace Gsw;

/**
 * Database configuration interface
 */
interface DatabaseConfigurationInterface
{
    /**
     * Get database host
     * @return string host name
     */
    public function getHost(): string;

    /**
     * Get database username
     * @return string username
     */
    public function getUsername(): string;

    /**
     * Get database password
     * @return string password
     */
    public function getPassword(): string;

    /**
     * Get database name
     * @return string database name
     */
    public function getDatabase(): string;
}