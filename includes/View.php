<?php 

namespace Gsw;

use Gsw\User;
use Gsw\ViewInterface;

class View implements ViewInterface
{
    /**
     * @var string View layout name
     */
    protected $layout = DEFAULT_LAYOUT;

    /**
     * Render output html with given data
     * 
     * @param string $view Viev file path, example: articles/index
     * @param array $args Data, example ['page_title'="Example Page!"]
     * @param bool $with_layout Render layout file?
     * @return void
     */
    public function render(string $view, array $args = [], bool $with_layout = true): void
    {
        $file_path = VIEWS_PATH . $view . '.php';
        
        if (file_exists($file_path)) {
            extract($args);

            if ($with_layout) {
                ob_start();
                require $file_path;
                $content = ob_get_contents();
                ob_get_clean();

                require VIEWS_PATH . 'layouts/' . $this->layout . '.php';
            } else {
                require $file_path;
            }
        } else {
            throw new \Exception("View file doesn't exists!", 500);
        }
    }

    /**
     * Set layout file, main by default
     * @param string $name Layout filename
     * @return void
     */
    public function setLayout(string $name): void
    {
        $this->layout = $name;
    }
}